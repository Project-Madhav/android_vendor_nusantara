# Packages
PRODUCT_PACKAGES += \
    LiveWallpapers \
    LiveWallpapersPicker \
    DeskClock \
    CustomDoze \
    Gallery2 \
    QuickAccessWallet \
    messaging \
    WallpaperPicker2 \
    ThemePicker \
    MadhavPapers \
    Stk \
    SimpleDeviceConfig

# Allows registering device to Google easier for gapps
# Integrates package for easier Google Pay fixing
PRODUCT_PACKAGES += \
    sqlite3
